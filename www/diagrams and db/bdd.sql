-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le :  sam. 28 juil. 2018 à 07:54
-- Version du serveur :  10.1.34-MariaDB
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `teaihnct_opcP5`
--

-- --------------------------------------------------------

--
-- Structure de la table `comment`
--

CREATE TABLE `comment` (
  `commentID` int(11) NOT NULL,
  `postID` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(64) NOT NULL,
  `comment` text NOT NULL,
  `commentDate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `comment`
--

INSERT INTO `comment` (`commentID`, `postID`, `name`, `email`, `comment`, `commentDate`) VALUES
(1, 2, 'Léo', '', 'Super article je vais installer la dernière version de php7 donc !! :)\r\nMerci', '2018-06-06'),
(2, 3, 'valentin', '', '<p>je vois que sur ton archi tu utilises un reverse-proxy, nginx, comme load-balancer, et que tu n\'évoques pas la possibilité de la gestion du cache dès cette étape (du reverse-proxy) ... est-ce une volonté de ne pas le gérer dès ce stade ? Si oui pourrais-tu nous parler de tes non-motivations et sinon aurais-tu des préconisations (profiter de nginx déjà installé, varnish, autre ...) ?</p>', '2018-05-16'),
(3, 1, 'Gérard', '', 'super article ! Merci pour tous ces détails ', '2018-06-21'),
(4, 2, 'Tom', '', 'Merci pour cet article !', '2018-07-05');

-- --------------------------------------------------------

--
-- Structure de la table `picture`
--

CREATE TABLE `picture` (
  `pictureID` int(11) NOT NULL,
  `pictureName` varchar(255) NOT NULL,
  `picturePost` varchar(255) NOT NULL,
  `pictureDate` date NOT NULL,
  `pictureSource` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `post`
--

CREATE TABLE `post` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `content` text CHARACTER SET utf8 NOT NULL,
  `creationDate` date NOT NULL,
  `lastModification` date DEFAULT NULL,
  `idAuthor` tinyint(1) NOT NULL,
  `summary` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `post`
--

INSERT INTO `post` (`id`, `title`, `content`, `creationDate`, `lastModification`, `idAuthor`, `summary`) VALUES
(1, 'RGPD et e-commerce : quels changements dans les campagnes emailing ?', '<p>Le 25 mai de cette année, le nouveau règlement général sur la protection des données (RGPD) entrera en vigueur et remplacera les réglementations existantes. Les entreprises du e-commerce devront ajuster leurs pratiques d’ici là. Aujourd’hui, les spécialistes de Newsletter2Go, le logiciel emailing pour les e-commerçants, vous expliquent tout ce que vous devez savoir pour vous préparer au RGPD.</p>\r\n\r\n<p><a href=\"#\"><img class=\"imgPost\" src=\"img/rgpd.jpg\" alt=\"\"></a></p>\r\n\r\n            <p>Qu’est-ce que le RGPD et quel est son objectif  ?\r\n            Jusqu’à présent, chaque État membre de l’UE avait ses propres lois sur la confidentialité et la sécurité des données utilisateurs. Le nouveau RGPD assure l’unification de ces règlements afin de mieux protéger les citoyens européens.  \r\n\r\n            L’harmonisation de la législation au sein de l’Union européenne profite avant tout aux consommateurs. À l’avenir, ceux-ci évolueront dans le même cadre juridique et ne devront plus se familiariser avec les lois de chaque État membres. Pour les entreprises qui opèrent à l’international, cela conduit à une simplification des processus internes et garantit généralement une plus grande efficacité, puisque désormais les clients de différents pays peuvent être traités de manière égale.</p>\r\n\r\n            <p>À qui s’applique le RGPD ?\r\n            Le RGPD s’applique à toute organisation ou entreprise qui traite des données personnelles. « Traiter » signifie ici la transmission, le stockage, l’analyse ou l’utilisation de données qui permettent d’identifier une personne physique (comme le nom, la date de naissance ou l’adresse). L’adresse IP peut également être considérée comme une donnée personnelle car elle peut être affectée à une personne spécifique.</p>\r\n\r\n            <p>Le RGPD: qu’est-ce que cela change pour les e-commerçants ?\r\n          \r\n            Si vous avez une boutique en ligne, voici les principes essentiels que vous devez prendre en compte :\r\n\r\n            Le consentement. Vous devez toujours avoir le consentement explicite des personnes dont vous traitez les données. Si vous avez une boutique en ligne et envoyez régulièrement des newsletters à vos clients, cela signifie que chaque destinataire doit vous donner son accord explicite avant que vous ne l’inscriviez à votre carnet d’adresses. De plus, les personne concernées doivent être en mesure de retirer leur consentement à tout moment.\r\n \r\n\r\n            Le principe de minimisation des données collectées. Le traitement des données doit être limité à l’objectif poursuivi lors de la collecte initiale. Dans le formulaire d’inscription à votre newsletter, demandez uniquement les informations réellement nécessaires pour envoyer des newsletters aux destinataires. Des informations supplémentaires telles que le nom ou la date de naissance peuvent être demandées, mais ne devraient pas être obligatoires. De plus, n’utilisez jamais les données de vos clients pour autre chose que la raison pour laquelle vous avez collecté les données au départ.\r\n \r\n\r\n            La transparence. Vos clients vous confient leurs données et doivent donc savoir exactement quelles sont les données collectées sur eux et surtout ce qu’il advient de ces données par la suite. Si un client a des questions à propos de ses données personnelles, vous êtes tenu, en tant que société, de fournir des informations à ce sujet.</p>\r\n            <p>Quel changements pour vos campagnes emailing ?\r\n            Pour les emails marketeurs, un certain nombre de choses changent avec le RGPD. Ils doivent, par exemple, être beaucoup plus prudents lorsqu’il est question de consentement. Ainsi, ils doivent mettre en place un système d’inscription double opt-in et pouvoir prouver l’accord explicite de leur destinataires. Il est donc grand temps de dire adieu aux cases pré-cochées et aux inscriptions simple opt-in !\r\n\r\n            Soyez prudent avec vos anciennes adresses et assurez-vous que vous avez l’autorisation de chaque destinataire. Évitez également à tout prix l’achat de listes d’adresses mails en masse, car  le consentement que vous collectez devra être donné de façon « libre, spécifique, éclairée et univoque » selon l’article 4 du règlement. Par ailleurs, pensez à garder une trace du consentement de vos contacts en cas de litige (horodatage et adresse IP notamment).\r\n\r\n            Dans le cadre du marketing automation, vous devrez informer vos destinataires des données qui seront utilisées dans un scénario automatisé. Vous comptez envoyer des emailings d’anniversaire ou des recommandations produit ? Vos destinataires devront être informés que leur date d’anniversaire ou leur comportement d’achat seront utilisés pour segmenter vos envois.</p>\r\n          \r\n           \r\n            <p>Source Nicolas Hachet</p>', '2018-04-24', '0000-00-00', 1, '<img class=\"img-fluid\" src=\"img/rgpdSummary.jpg\" alt=\"\">Nouveau règlement général sur la protection des données (RGPD) du 25 mai 2018.Le 25 mai de cette année, le nouveau règlement général sur la protection des données (RGPD) entrera en vigueur et remplacera les réglementations existantes. Les entreprises du e-commerce devront ajuster leurs pratiques d’ici là.'),
(2, 'PHP 7 : la nouvelle version de PHP au crible', '<p>Quelles sont les principales évolutions de la nouvelle version du langage de script serveur ? Quel apport en termes de performance ? Le dossier de la rédaction sur PHP 7.</p>\r\n\r\n<p>La version finale de PHP 7 est sortie en décembre 2015 (lire l\'annonce officielle sur php.net). Elle est livrée avec une nouvelle version du Zend Engine. Un manuel de migration a été publié par le site du projet open source.</p>\r\n\r\n<p>Le projet PHP vient par ailleurs de livrer PHP 7.0.12 - qui corrige plusieurs bugs de sécurité. La Release Candidate 6 de PHP 7.1 est aussi disponible. PHP 7.1 a pour but d\'introduire quelques éléments complémentaires à PHP 7 : le support des Server Push via curl HTTP/2 et des types nullables par exemple (voir la liste complète des évolutions sur le site du projet PHP). Les performances du langage doivent aussi être optimisées pour l\'occasion.</p>\r\n\r\n<p>Quid de la performance de PHP 7\r\n\"PHP 7 est plus rapide.\" C\'est la principale promesse faite par Zend et les contributeurs principaux du projet PHP. Cette nouvelle version est basée sur PHPNG (pour PHP Next-Generation). Une initiative qui a été lancée par Zend en réponse à la technologie HHVM de Facebook, qui avait pour but de proposer une version de PHP qui se voulait plus performante.</p>\r\n\r\n<p>Selon Zend, la mise à jour des applications vers PHP 7 pourrait engendrer un surcroît de performance de 25% à 70%. L\'éditeur a publié quelques indicateurs qu\'il a résumé en une infographie publiée en mai 2015. Ces comparatifs montrent que WordPress (en version 4.1) serait deux fois plus rapide avec PHP 7 qu\'avec PHP 5.6, et Drupal (7) 70% plus rapide. A travers son benchmark, le projet PHP met aussi en avant un niveau d\'optimisation qui se veut être au même niveau que celui de HHVM, voire légèrement au dessus.</p>\r\n\r\n<p>Premier Benchmark du JDN\r\nSelon les premiers comparatifs réalisés par CCM Benchmark, l\'éditeur du JDN, les gains (entre PHP 5.6 et PHP 7) peuvent atteindre 50% en temps d\'exécution CPU, et près de 50% également en consommation de mémoire. \"C\'est au-dessus de ce que nous avions anticipé. Je n\'ai jamais vu un tel différentiel de performance lors des précédentes montées de version\", note Xavier Leune, responsable Framework PHP chez CCM Benchmark.</p>\r\n\r\n<p>Les nouveautés de PHP 7\r\nZend avait diffusé en mars dernier une infographie qui présentait quelques évolutions de PHP 7. Elle mentionnait les Spaceships qui font apparaitre un nouvel opérateur de comparaison <=>. Il pourra être utilisé pour combiner des comparaisons (un décryptage en français ici).</p>\r\n\r\n<p>Autre évolution évoquée, les Return Type Declarations & Scalar Type Hints permettent de déclarer (de manière optionnelle) un type de retour pour les fonctions et méthodes. Les Type Hinst comme cette nouvelle déclaration peuvent en outre prendre en charge les types scalaires (pour préciser le retour attendu en matière de nombre ou chaîne de caractères). \"C\'est très intéressant. Cela va contribuer à simplifier la gestion de la cohérence des entrées et sorties\", commente Xavier Leune.</p>\r\n\r\n<p>Pourquoi PHP est-il passé de la version 5 à la version 7 ?</br>\r\nEn juillet 2014, les contributeurs du projet PHP se sont prononcés sur un passage direct de PHP 5.x à PHP 7.x (voir le wiki du projet). En fait, PHP 6 a bien existé, mais il est resté à l\'état de chantier. Cette version devait conduire à l\'intégration de l\'Unicode au langage (en vue de faciliter la manipulation de texte dans différentes langues). Mais face aux trop nombreuses difficultés techniques, l\'initiative a été abandonnée. Les autres évolutions envisagées dans PHP 6 ont depuis été réintégrées à de nouvelles versions intermédiaires (PHP 5.3 et 5.4). C\'est pour éviter la confusion qu\'est née alors cette idée de passer directement à la version 7.</p>\r\n\r\n<p>Enfin pour les partisans de PHP 7, le chiffre 6 a aussi trop souvent été associé à la notion d\'échec dans le monde des environnements de développement web : \"PHP 6 a été un échec ; Perl 6 a été un échec. Il est en fait associé à l\'échec, même en dehors du monde des langages dynamiques : MySQL 6 existait, mais n\'a jamais été publié.\"</p>', '2018-06-05', '0000-00-00', 1, '<img class=\"img-fluid\" src=\"img/php7.jpg\" alt=\"\">Quelles sont les principales évolutions de la nouvelle version du langage de script serveur ? Quel apport en termes de performance ? Le dossier de la rédaction sur PHP 7.'),
(3, 'Architecture PHP scalable et haute performance en 2018', '<p><img src=\"img/architecturePhpScalable.png\"></p>\r\n<p>C’est l’heure de mettre à jour cette architecture PHP en version 2018 ! Il s’agit d’un article contenant quelques pistes sur les principaux composants logiciels à utiliser afin d’obtenir des performances optimales sur votre socle PHP.</p>\r\n\r\n<p>Sans plus attendre, voici donc une architecture PHP autorisant la montée en charge, les hautes performances et la scalabilité horizontale. C’est à dire la capacité à ajouter des serveurs pour encaisser des pics d’utilisation.</p></br>\r\n            \r\n<p>On remarque que le dispositif est articulé autour du composant Nginx qui joue le rôle de load balancer, de serveurs PHP-FPM et d’un composant Redis. Je vous détaille tout ça juste après.</p>\r\n\r\n<p>Nginx pour le load balancing\r\nConcrètement, le pivot de cette architecture haute performance est l’utilisation d’un load balancer (ou répartiteur de charge en bon français) comme point d’entrée : le front-end. L’idée est de pouvoir répartir la charge entre un ou plusieurs serveurs applicatifs qui traiterons la requête cliente : les back-ends. Le load balancer se charge de déterminer le bon back-end selon une stratégie qu’il est possible de configurer : poids donné au back-end, round robin, least-connections, ip-hash… Sur le schéma, vous voyez la présence d’un serveur Nginx. Rien ne vous empêche de le remplacer par un serveur HAProxy ou même par un serveur Apache… Nginx ou HAProxy.</p>\r\n\r\n<p>PHP-FPM 7 pour le traitement des requêtes\r\nPHP-FPM (pour PHP FastCGI Process Manager) a le très gros intérêt d’être complètement découplé du serveur HTTP qui l’utilise. Ainsi, peu importe que vous utilisiez Apache, Nginx ou HAProxy comme load balancer, le backend PHP-FPM est le même.</p>\r\n\r\n<p>L’organisation d’un backend est la suivante :</p>\r\n\r\n<p>Nginx et PHP-FPM  sont installés sur chaque backends ;\r\nPHP-FPM utilise plusieurs workers pour traiter simultanément des requêtes sur chaque backend ;\r\nL’application PHP est accessible par tous les workers via la configuration Nginx du backend ;\r\nIl est possible d’installer autant de backends que nécessaire pour gérer la charge de connexions.\r\nOptez pour PHP 7 pour obtenir des performances optimales. PHP-FPM peut également tourner sur le même serveur que Nginx si vous n’avez pas besoin du load-balancing.</p>\r\n\r\n<p>L’idée de cette architecture est de prendre en entrée une requête fournie par le serveur HTTP et de rendre une réponse qui sera renvoyée par ce dernier. Le load balancer ne s’occupe que de choisir le bon serveur applicatif. La vraie complexité est de gérer les sessions utilisateur qui, de base, sont gérées via le système de fichiers du serveur. Dans notre cas, c’est impossible puisque l’utilisateur peut être dirigé vers un serveur A pour sa première requête, vers un serveur B pour sa seconde, etc. C’est là que le prochain composant revêt toute son importance.</p>\r\n\r\n<P>Redis pour gérer les sessions\r\nQuand vous avez plusieurs backends en stratégie round-robin ou least-connections (les plus fréquents), un même client sera redirigé vers un serveur différent d’une requête à l’autre. Il n’est donc pas possible de créer une adhérence forte entre le serveur et le stockage des sessions (en système de fichiers par exemple). Dans ce cas, Redis est le composant parfait pour traiter le problème en fournissant un serveur de cache distribué qui permettra de centraliser l’ensemble de vos sessions, peu importe le serveur qui les créent ou les utilisent. Comme les sessions utilisateurs sont liées à un jeton placé sur chaque requête (par défaut c’est un cookie), peu importe le back-end qui recevra le jeton, il pourra retrouver les données des sessions sur le même serveur Redis. A noter que des solutions distribuées comme memcached fonctionnent également très bien !\r\nAutre solution : si vous n’avez pas besoin de stocker des informations entre chaque requête, supprimez les sessions ! Vous pouvez sans problème gérer des requêtes stateless grâce à un jeton ajouté à celles-ci. Pour ça, je vous invite à aller faire quelques recherches sur les JSON Web Tokens (JWT pour les intimes).</p>\r\n\r\n<p>MysQL, Maria DB, MongoDB…\r\nPour la partie stockage des données, mon avis est de moins en moins tranché. Les moteurs de base de données actuels sont performants et selon qui vous souhaitiez gérer vos données en relationnel ou en NoSQL, vous y trouverez votre compte. Sachez également que les hautes performances sur de gros volumes de données ne sont jamais magiques. Il existe des techniques qui permettent d’optimiser le traitement et la récupération des données :</p></br>\r\n\r\n<p>Indexes</br>\r\nDénormalisation</br>\r\nRéplication</br>\r\nPrécaclcul de données</br>\r\nTraitements asynchrones</br>\r\nMoteur d’indexation type Elasticsearch\r\netc.</p></br>\r\n\r\n<p>Bref, selon votre applicatif et le volume de données à gérer, des solutions différentes seront possibles pour votre architecture. Notez bien que la mise en cluster sera probablement un prérequis au choix de votre SGBD.</p>\r\n\r\n<p>Conclusion :</br>\r\nJe vous ai présenté ici une architecture simple et rapide à mettre en oeuvre permettant de gérer un site Web fort trafic. Pour améliorer encore les performances et selon les fonctionnalités de l’application, il est possible d’ajouter des composants comme NodeJS ou RabbitMQ pour les traitements asynchrones. Typiquement, vous pouvez exécuter en asynchrone tous les traitements qui ne nécessitent pas un retour immédiat : envoi de mails, génération de fichiers, exports, etc. Ce principe basé sur une architecture distribuée fera l’objet d’un prochain article.</p>\r\n<p>Source Nicolas Hachet</p>', '2018-06-10', '0000-00-00', 1, '<img class=\"img-fluid\" src=\"img/phpSecale.jpg\">C’est l’heure de mettre à jour cette architecture PHP en version 2018 ! Il s’agit d’un article contenant quelques pistes sur les principaux composants logiciels à utiliser afin d’obtenir des performances optimales sur votre socle PHP.'),
(4, 'Symfony : le framework PHP open source made in France', '<p>Qu’est-ce qu’un framework ?\r\n\r\nPour développer une plateforme web (site web, extranet, e-commerce …), vous avez le choix entre :\r\n\r\n- Un développement maison de A à Z en utilisant le langage dans son plus simple appareil, en démarrant d’une page blanche\r\n\r\n- Utiliser un CMS du marché si votre projet correspond bien aux fonctionnalités proposées\r\n\r\n-Ou utiliser un framework, entre le développement de zéro et le CMS</p>\r\n\r\n<p>Un framework PHP est constitué de plusieurs composants qui cohabitent parfaitement entre eux et forment une base de développement solide.</p>\r\n\r\n<p>Quels avantages à utiliser un framework comme Symfony ?\r\n\r\nGrâce aux normes et conventions que chaque développeur sur un projet Symfony doit respecter, on obtient une organisation solide des fichiers et du code. Les avantages sont multiples :\r\n\r\nLes développeurs Symfony qui maîtrisent le framework pourront facilement intégrer un projet développé à partir du framework, contrairement à un projet développé en PHP “maison”, où il n’y’a pas de normes ni règles imposées. Dans ce dernier cas, la phase d’apprentissage et reprise du code existant peut demander un effort conséquent pour le nouveau développeur intégrant le projet.\r\n\r\nLes fichiers doivent respecter une syntaxe particulière et doivent se trouver au bon endroit dans l’arborescence du projet. Cela garantit une facilité de maintenance sur le long terme, les développeurs savent rapidement dans quel fichier il faut aller pour apporter des modifications.\r\n\r\nL’architecture MVC (Modèle Vue Contrôleur) permet de découper le code représentant la logique métier de l’application et le code de présentation des vues. Ainsi, un intégrateur web voir même un webdesigner n’aura aucun mal à intervenir sur la partie présentation (vues) du projet, sans avoir à intervenir sur des fichiers PHP complexes.\r\n\r\nFavorise la réutilisation de code, la création de tests automatisés (tests unitaires avec phpUnit ou Atoum et tests fonctionnels avec phpUnit ou Behat) et le respect des recommandations PHP-FIG (Des recommandations mondiales pour une meilleure interopérabilité entre les projets web PHP). Symfony permet donc de produire du code de qualité.</p>\r\n\r\n<p>Symfony est-il adapté à votre projet ?\r\n\r\nEn pratique, le framework peut répondre à tous types de projets web, API, outils métiers …</p>\r\n\r\n<p>Concevoir des landing pages promotionnelles avec Symfony ? \r\nNon !\r\n\r\n- Que ce soit pour réaliser des landing pages ou des sites statiques, Symfony n’est pas forcément la bonne solution. Dans ce cas précis, il est plus intéressant de partir sur un micro-framework comme Silex (basé sur les composants Symfony) qui aura l’avantage de proposer un système de routing dynamique, de les concevoir en PHP maison ou en HTML/CSS/JS statique.</p>\r\n\r\n<p>Utiliser Symfony pour la création d’un site vitrine ? \r\nOui et non ...\r\n\r\n- Tout dépend du type de site vitrine, si ce dernier apporte une dimension dynamique plus complexe qu’une gestion d’articles, de pages de contenus ou qu’un formulaire de contact, il peut y avoir un intérêt. Par exemple, si vous adossez un système d’espace client, une interconnexion avec un système informatique comme un ERP / CRM, un formulaire avancé, du paiement en ligne … Dans ces cas, il peut y avoir une plus-value à utiliser Symfony.\r\n\r\nDans le cas contraire, privilégiez l’utilisation d’un CMS comme Wordpress, Drupal (dont la version 8 repose sur les composants Symfony), ou encore Bolt, un CMS basé sur Symfony (qui vous permettra plus de souplesse pour vos évolutions futures).</p>\r\n\r\n<p>Pour le développement d’un espace connecté de type Intranet ou Extranet ? \r\nOui !\r\n\r\n- Symfony répond totalement au besoin pour la création d’intranet ou extranet. Le système de rôles et de gestion des utilisateurs embarqué permet de développer rapidement une plateforme de ce type avec personnalisation des droits d’accès et rôles de chacun…</p>\r\n\r\n<p>Dans le cadre de la réalisation d’un outil métier avec des workflows complexes ? \r\nOui !\r\n\r\n- Pour réaliser des outils métiers, que ce soit des ERP ou CRM sur mesure, comme des outils sur mesure pour optimiser des tâches quotidiennes dans une entreprise, Symfony est parfaitement adapté.\r\n\r\nOn dit souvent de Symfony qu’il s’agit d’un des frameworks PHP open source les plus adapté pour le monde professionnel de part sa robustesse, sa stabilité et sa modularité (très important pour ce type de projet en constante évolution), ce qui en fait un framework de choix pour ce type de projet.</p>\r\n\r\n<p>Pour créer une API ou des webservices ? \r\nOui !\r\n\r\n- Symfony étant basé sur une architecture HTTP, son utilisation est idéale pour créer des webservices RESTful. Tout est natif dans le framework, de la conception des endpoints jusqu’à la sécurité des accès. Il est possible d’aller plus loin grâce à des bundles open source dédiés qui pourront être intégrés dans le projet.</p>\r\n\r\n<p>Et pour une plateforme avec de fortes contraintes de temps réel ? \r\nOui et non …\r\n\r\n- Si le projet demande des échanges temps réel comme un système de chat live, une plateforme de jeux vidéos, de statistiques financières / bourse … Symfony n’est pas la solution la plus adaptée. Il est préférable de partir sur une solution de type Node.js / Socket.io qui pourra traiter plus facilement cette problématique.\r\n- Toutefois, Symfony peut être utilisé comme socle backend et couplé à une API, tout en utilisant pour la partie frontend des technos plus adaptées comme Node, React.js ou Angular.js.</p>\r\n\r\nSource \" Novaway\"', '2018-06-26', '0000-00-00', 1, '<img class=\"img-fluid\" src=\"img/symfony.jpg\" alt=\"\">Qu’est-ce qu’un framework ?<br/>\r\nQuels avantages à utiliser un framework comme Symfony ?<br/>\r\nSymfony est-il adapté à votre projet ?');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `userID` int(11) NOT NULL,
  `firstName` varchar(255) NOT NULL,
  `lastName` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `lastLogin` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`userID`, `firstName`, `lastName`, `email`, `login`, `password`, `role`, `active`, `lastLogin`) VALUES
(1, 'Karine', 'Gieudes', 'gieudes.karine@gmail.com', 'kat63', 'blogamoi', 'admin', 1, '2018-07-23 08:58:16'),
(2, 'Mentor', 'Daouda', 'daoudi5@gmail.com', 'mentor', 'supermentor', 'membre', 1, '2018-07-02 05:39:54'),
(3, 'Mentor', 'soutenance', 'jexamine@gmail.com', 'mentor', 'supermentor', 'examinateur', 0, '2018-07-09 14:40:19');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `comment`
--
ALTER TABLE `comment`
  ADD PRIMARY KEY (`commentID`);

--
-- Index pour la table `picture`
--
ALTER TABLE `picture`
  ADD PRIMARY KEY (`pictureID`);

--
-- Index pour la table `post`
--
ALTER TABLE `post`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`userID`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `comment`
--
ALTER TABLE `comment`
  MODIFY `commentID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `picture`
--
ALTER TABLE `picture`
  MODIFY `pictureID` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `post`
--
ALTER TABLE `post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `userID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
