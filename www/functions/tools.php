<?php
function getUserName($user){
  global $bdd;
  $reqUser = $bdd->query("SELECT `firstName` FROM `user` WHERE `userID`=$user");
  while ($dataUser = $reqUser->fetch()){
    $user= $dataUser['firstName'];
  }
  $reqUser->closeCursor();
  return $user;
}

function checkInput($input){
  $allowedTags  = '<p><strong><em><u><h1><h2><h3><h4><h5><h6><img>';
  $allowedTags .= '<li><ol><ul><span><div><br>';
  return strip_tags(stripslashes($input),$allowedTags);
}
?>